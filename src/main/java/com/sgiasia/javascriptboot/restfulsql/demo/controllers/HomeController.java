package com.sgiasia.javascriptboot.restfulsql.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class HomeController {

    @RequestMapping("/")
    public String welcome(){
        return "Welcome to Spring REST API";
    }

    @RequestMapping("/hello")
    public String hello(){
        return "Hello! You're accessing /hello path";
    }
}
