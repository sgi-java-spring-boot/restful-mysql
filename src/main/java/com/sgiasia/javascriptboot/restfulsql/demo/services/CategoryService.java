package com.sgiasia.javascriptboot.restfulsql.demo.services;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Category;
import com.sgiasia.javascriptboot.restfulsql.demo.models.repos.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryRepo categoryRepo;

    public Category save(Category category){
        return categoryRepo.save(category);
    }

    public Category findOne(Long id){
        return categoryRepo.findById(id).get();
    }

    public Iterable<Category> findAll(){
        return categoryRepo.findAll();
    }

    public void removeOne(Long id){
        categoryRepo.deleteById(id);
    }
}
