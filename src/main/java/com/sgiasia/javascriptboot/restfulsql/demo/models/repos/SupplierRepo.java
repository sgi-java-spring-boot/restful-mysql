package com.sgiasia.javascriptboot.restfulsql.demo.models.repos;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Supplier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SupplierRepo extends CrudRepository<Supplier, Long> {
    Supplier findByEmail(String email);
    List<Supplier> findByAddressContaining(String keyword);

}
