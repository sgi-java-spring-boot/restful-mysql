package com.sgiasia.javascriptboot.restfulsql.demo.controllers;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Product;
import com.sgiasia.javascriptboot.restfulsql.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping
    public Iterable<Product> getAllProduct(){
        return productService .findAll();
    }

    @GetMapping("/{id}")
    public Product getOneProduct(@PathVariable("id") Long id){
        return  productService.findOne(id);
    }

    @GetMapping("/name/{keyword}")
    public List<Product> findProductByName(@PathVariable("keyword") String keyword){
        return productService.findByName(keyword);
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public void saveProduct(@RequestBody Product product){
        productService.save(product);
    }

    @DeleteMapping
    @Secured("ROLE_ADMIN")
    public void deleteProduct(Long id){
        productService.removeOne(id);
    }

}
