package com.sgiasia.javascriptboot.restfulsql.demo.dto;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Product;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter @Setter @NoArgsConstructor
public class SupplierData {
    @NotEmpty(message = "Name is required")
    @Size(min=5)
    private String name;
    @NotEmpty(message = "Address is required")
    private String address;
    @NotEmpty(message = "Email is required")
    @Email(message = "Email is not valid")
    private String email;

    public SupplierData(String name, String address, String email) {
        this.name = name;
        this.address = address;
        this.email = email;
    }
}
