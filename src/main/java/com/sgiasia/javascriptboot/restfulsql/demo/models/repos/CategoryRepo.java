package com.sgiasia.javascriptboot.restfulsql.demo.models.repos;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepo extends CrudRepository<Category, Long> {
}
