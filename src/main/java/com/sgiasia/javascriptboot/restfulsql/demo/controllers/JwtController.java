package com.sgiasia.javascriptboot.restfulsql.demo.controllers;

import com.sgiasia.javascriptboot.restfulsql.demo.helpers.JwtHelper;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JwtController {

    private JwtHelper jwtHelper = new JwtHelper();

    @GetMapping("/encodejwt")
    public String encodeJwt(){


        UserDetails user =
                User.withUsername("user")
                        .password("{noop}password")
                        .roles("USER")
                        .build();

        return jwtHelper.generateToken(user);
    }


    @GetMapping("/decodejwt")
    public String decodeJwt(@RequestParam String token){
        return jwtHelper.getUsernameFromToken(token);
    }
}
