package com.sgiasia.javascriptboot.restfulsql.demo.controllers;

import com.sgiasia.javascriptboot.restfulsql.demo.dto.ResponseData;
import com.sgiasia.javascriptboot.restfulsql.demo.dto.SupplierData;
import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Supplier;
import com.sgiasia.javascriptboot.restfulsql.demo.services.SupplierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/suppliers")
@Secured("ROLE_ADMIN")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    Logger logger = LoggerFactory.getLogger(SupplierService.class);

    @GetMapping
    public ResponseEntity<ResponseData<Iterable<SupplierData>>> findAll(){
        ResponseData<Iterable<SupplierData>> responseData = new ResponseData<Iterable<SupplierData>>();

        Iterable<Supplier> suppliers =  supplierService.findAll();
        List<SupplierData> supplierData = new ArrayList<SupplierData>();
        for(Supplier s : suppliers){
            SupplierData tempData = new SupplierData();
            tempData.setName(s.getName());
            tempData.setAddress(s.getAddress());
            tempData.setEmail(s.getEmail());
            supplierData.add(tempData);
        }

        responseData.setStatus(true);
        responseData.setPayload(supplierData);
        responseData.getMessages().add("Ok");
        return ResponseEntity.ok().body(responseData);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<SupplierData>> findOne(@PathVariable("id") Long id){
        ResponseData<SupplierData> responseData = new ResponseData<SupplierData>();

        Supplier supplier =  supplierService.findOne(id);
        if(supplier == null) {
            responseData.setStatus(false);
            responseData.getMessages().add("Data tidak ditemukan");
            return ResponseEntity.badRequest().body(responseData);
        }

        logger.info(supplier.toString());
        SupplierData supplierData = new SupplierData();
        supplierData.setName(supplier.getName());
        supplierData.setAddress(supplier.getAddress());
        supplierData.setEmail(supplier.getEmail());
        responseData.setStatus(true);
        responseData.setPayload(supplierData);
        responseData.getMessages().add("Ok");

        return ResponseEntity.ok().body(responseData);
    }


    @GetMapping("/address/{keyword}")
    public ResponseEntity<ResponseData<List<SupplierData>>> findOne(@PathVariable("keyword") String keyword){
        ResponseData<List<SupplierData>> responseData = new ResponseData<List<SupplierData>>();

        Iterable<Supplier> suppliers =  supplierService.cariSuplierBerdasarkanKeywordAlamat(keyword);
        List<SupplierData> supplierData = new ArrayList<SupplierData>();
        for(Supplier s : suppliers){
            SupplierData tempData = new SupplierData();
            tempData.setName(s.getName());
            tempData.setAddress(s.getAddress());
            tempData.setEmail(s.getEmail());
            supplierData.add(tempData);
        }

        responseData.setStatus(true);
        responseData.setPayload(supplierData);
        responseData.getMessages().add("Ok");
        System.out.println("test");

        return ResponseEntity.ok().body(responseData);
    }

    @PostMapping
    public ResponseEntity<ResponseData<Supplier>> create(@RequestBody @Valid SupplierData supplierData,
                                                         Errors errors){
        ResponseData<Supplier> responseData = new ResponseData<Supplier>();

        if(errors.hasErrors()){
            for(ObjectError error: errors.getAllErrors()){
                responseData.getMessages().add(error.getDefaultMessage());
            }
            return ResponseEntity.badRequest().body(responseData);
        }

        Supplier supplier = new Supplier();
        supplier.setName(supplierData.getName());
        supplier.setAddress(supplierData.getAddress());
        supplier.setEmail(supplierData.getEmail());
        logger.info(supplier.toString());
        Supplier supplierResult = supplierService.save(supplier);


        responseData.setStatus(true);
        responseData.setPayload(supplierResult);
        responseData.getMessages().add("Success");
        return ResponseEntity.ok(responseData);
    }
}
