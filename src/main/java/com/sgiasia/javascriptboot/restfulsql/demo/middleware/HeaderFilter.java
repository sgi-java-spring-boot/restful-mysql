package com.sgiasia.javascriptboot.restfulsql.demo.middleware;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//@Component
public class HeaderFilter extends GenericFilterBean {
    Logger logger = LoggerFactory.getLogger(HeaderFilter.class);

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain
            filterChain)throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        //if header is missing , send un-athorized error back
        String authHeader = request.getHeader("x-api-key");
        if (StringUtils.isEmpty(authHeader)) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            String defaultToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIiLCJyb2xlIjoiVVNFUiIsImlhdCI6MTUxNjIzOTAyMn0.QfhSYdpILptmUaSiOe4TBEB9LgykqbsdD1S1jl4imnM";
            if(authHeader.equals(defaultToken)) {
                filterChain.doFilter(req, res);
            } else {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }
        logger.info("Header filtered");
    }
}

