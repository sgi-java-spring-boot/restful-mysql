package com.sgiasia.javascriptboot.restfulsql.demo.models.repos;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepo extends CrudRepository<Product, Long> {
    List<Product> findByNameContains(String name);
}
