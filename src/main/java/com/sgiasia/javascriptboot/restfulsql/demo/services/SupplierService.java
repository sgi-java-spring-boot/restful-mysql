package com.sgiasia.javascriptboot.restfulsql.demo.services;

import com.sgiasia.javascriptboot.restfulsql.demo.models.entities.Supplier;
import com.sgiasia.javascriptboot.restfulsql.demo.models.repos.SupplierRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SupplierService {
    Logger logger = LoggerFactory.getLogger(SupplierService.class);
    @Autowired
    private SupplierRepo supplierRepo;

    public Supplier save(Supplier supplier){
        logger.info(supplier.toString());
        return supplierRepo.save(supplier);
    }

    public Supplier findOne(Long id){
        Optional<Supplier> supplier = supplierRepo.findById(id);
        if(supplier.isPresent()){
            return  supplier.get();
        } else {
            return null;
        }
    }

    public Iterable<Supplier> findAll(){
        return supplierRepo.findAll();
    }

    public boolean removeOne(Long id){
        try {
            supplierRepo.deleteById(id);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public Supplier cariSuplierBerdasarkanEmail(String email){
        return supplierRepo.findByEmail(email);
    }

    public List<Supplier> cariSuplierBerdasarkanKeywordAlamat(String keyword){
        return supplierRepo.findByAddressContaining(keyword);
    }
}
